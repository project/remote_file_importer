<?php

namespace Drupal\remote_file_importer\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\remote_file_importer\DataSourcePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the styling profile settings form.
 */
class DataSourceForm extends EntityForm {

  use StringTranslationTrait;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The plugin manager.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a ProfileForm object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   * @param \Drupal\remote_file_importer\DataSourcePluginManager $pluginManager
   *   The plugin manager.
   */
  public function __construct(MessengerInterface $messenger, ConfigFactoryInterface $config, DataSourcePluginManager $pluginManager) {
    $this->messenger = $messenger;
    $this->config = $config;
    $this->pluginManager = $pluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('config.factory'),
      $container->get('plugin.manager.remote_file_importer.data_source')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\remote_file_importer\Entity\DataSource $dataSource */
    $dataSource = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $dataSource->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $dataSource->id(),
      '#required' => TRUE,
      '#machine_name' => [
        'exists' => [$this, 'dataSourceExists'],
        'replace_pattern' => '[^a-z0-9_.]+',
      ],
    ];

    $options = [];
    foreach ($this->pluginManager->getDefinitions() as $pluginId => $dataSourcePlugin) {
      $options[$pluginId] = $dataSourcePlugin['label'];
    }

    $form['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Data Source Type'),
      '#default_value' => $dataSource->plugin_id,
      '#required' => TRUE,
      '#options' => $options,
    ];

    if ($dataSource->plugin_id) {
      $form['plugin_id']['#disabled'] = TRUE;

      $dataSourcePlugin = $this->pluginManager->createInstance($dataSource->plugin_id);

      $form['settings'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Settings'),
      ] + $dataSourcePlugin->buildConfigurationForm($dataSource->settings);
    }

    $form['sync'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Syncronize with remote folder'),
      '#description' => $this->t('If checked, remote files that have been removed will also be removed from local file system.'),
      '#default_value' => $dataSource->sync,
    ];

    $form['keep_modify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Keep last modified date'),
      '#description' => $this->t('If checked, the downloaded files will keep the last modified date of the remote files.'),
      '#default_value' => $dataSource->keep_modify,
    ];

    $form['destination'] = [
      '#type' => 'container',
      '#attributes' => [
        'style' => 'display: flex; gap: 10px;',
      ],
    ];

    $form['destination']['destination_base'] = [
      '#type' => 'select',
      '#options' => [
        'private://' => 'private://',
        'public://' => 'public://',
        'temporary://' => 'temporary://',
      ],
      '#title' => $this->t('Destination base'),
      '#default_value' => $dataSource->destination_base ?? 'private://',
    ];

    $form['destination']['destination_folder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Destination folder'),
      '#default_value' => $dataSource->destination_folder ?? '',
      '#required' => TRUE,
      '#description' => $this->t('Directory where imported files get stored.'),
    ];

    $form['batch_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Batch size'),
      '#default_value' => $dataSource->batch_size ?? 0,
      '#required' => TRUE,
      '#description' => $this->t('Number of files to be downloaded per batch. If set to 0 all files will be downloaded in one sinlge batch.'),
    ];

    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $dataSource->active,
    ];

    if ($dataSource->id()) {
      $form['id']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $this->entity->set('label', trim((string) $form_state->getValue('label')));
    $this->entity->set('id', $form_state->getValue('id'));
    $this->entity->set('destination_base', $form_state->getValue('destination_base'));
    $destinationFolder = trim((string) $form_state->getValue('destination_folder'));
    $destinationFolder = trim($destinationFolder, '/');
    $this->entity->set('destination_folder', $destinationFolder);
    $this->entity->set('active', $form_state->getValue('active'));
    $this->entity->set('sync', $form_state->getValue('sync'));
    $this->entity->set('keep_modify', $form_state->getValue('keep_modify'));
    $this->entity->set('plugin_id', $form_state->getValue('plugin_id'));
    $this->entity->set('batch_size', $form_state->getValue('batch_size'));

    if ($this->entity->get('plugin_id')) {
      $dataSourcePlugin = $this->pluginManager->createInstance($this->entity->get('plugin_id'));
      $settings = $dataSourcePlugin->processConfigurationForm($form, $form_state, $this->entity);
      $this->entity->set('settings', $settings);
    }

    $status = $this->entity->save();

    // Tell the user we've updated/added the data source.
    $action = $status == SAVED_UPDATED ? 'updated' : 'added';
    $this->messenger()->addStatus($this->t(
      'Data Source %label has been %action.',
      ['%label' => $this->entity->label(), '%action' => $action]
    ));
    $this->logger('styling_profiles')
      ->notice(
        'Data Source profile %label has been %action.',
        ['%label' => $this->entity->label(), '%action' => $action]
      );

    // Redirect back to the list view if data source has been updated.
    if ($status == SAVED_UPDATED) {
      $form_state->setRedirect('entity.rfi_data_source.collection');
    }
    else {
      $form_state->setRedirect('entity.rfi_data_source.edit_form', [
        'rfi_data_source' => $this->entity->get('id'),
      ]);
    }

  }

  /**
   * Checks if a data source name is taken.
   *
   * @param string $value
   *   The machine name.
   * @param array $element
   *   An array containing the structure of the 'id' element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return bool
   *   Whether or not the data source name is taken.
   */
  public function dataSourceExists($value, array $element, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $profile */
    $dataSource = $form_state->getFormObject()->getEntity();
    return (bool) $this->entityTypeManager->getStorage($dataSource->getEntityTypeId())
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition($dataSource->getEntityType()->getKey('id'), $value)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $dataSourcePlugin = $this->pluginManager->createInstance($this->entity->plugin_id);
    $dataSourcePlugin->validateConfigurationForm($form, $form_state);
  }

}
