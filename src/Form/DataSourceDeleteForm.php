<?php

namespace Drupal\remote_file_importer\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to delete an Example.
 */
class DataSourceDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $this->messenger()->addMessage($this->t('Data Source %label has been deleted.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect('entity.rfi_data_source.collection');
  }

}
