<?php

namespace Drupal\remote_file_importer\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\remote_file_importer\DataSourcePluginManager;
use Drupal\remote_file_importer\Event\FilesToImportEvent;
use Drupal\remote_file_importer\Event\RemoteFileImporterEvents;
use Drupal\remote_file_importer\ImportHelperTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * ImportService for importing files from data sources.
 */
class ImportService {

  use ImportHelperTrait;
  use StringTranslationTrait;

  /**
   * Entity type service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Configuration.
   *
   * @var \Drupal\config
   */
  private $config;

  /**
   * Plugin manager.
   *
   * @var \Drupal\remote_file_importer\DataSourcePluginManager
   */
  private $pluginManager;

  /**
   * Event Dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * Batch definition.
   *
   * @var array
   */
  private $batch;

  /**
   * Data Sources.
   *
   * @var array
   */
  private $dataSources;

  /**
   * Constructs a new ImportService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system service.
   * @param \Drupal\remote_file_importer\DataSourcePluginManager $pluginManager
   *   Plugin manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   Event dispatcher.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    DataSourcePluginManager $pluginManager,
    EventDispatcherInterface $eventDispatcher,
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->pluginManager = $pluginManager;
    $this->eventDispatcher = $eventDispatcher;
    $this->prepareBatch();
    $this->dataSources = [];
  }

  /**
   * Prepares batch.
   */
  private function prepareBatch() {
    $this->batch = [
      'title' => $this->t('Importing files'),
      'operations' => [],
      'finished' => '\Drupal\remote_file_importer\ImportBatch::finished',
    ];
  }

  /**
   * Run batch.
   *
   * @param string $redirectRoute
   *   (Optional) Route to redirect after import.
   */
  private function runBatch($redirectRoute = NULL) {
    batch_set($this->batch);
    if ($redirectRoute) {
      return batch_process(Url::fromRoute($redirectRoute)->toString());
    }
    else {
      return drush_backend_batch_process();
    }
  }

  /**
   * Adds an operation to the to batch.
   *
   * If the batchsize of a data source is set, the files will be
   * split into chunks of the configured size.
   *
   * @param string $dataSourceId
   *   ID of data source.
   * @param array $items
   *   Array containing elements to be imported.
   */
  private function addOperationToBatch($dataSourceId, $items) {
    $dataSource = $this->entityTypeManager->getStorage('rfi_data_source')->load($dataSourceId);
    if (property_exists($dataSource, 'batch_size') && $dataSource->batch_size) {
      foreach (array_chunk($items, $dataSource->batch_size) as $chunkOfFiles) {
        $this->batch['operations'][] = [
          '\Drupal\remote_file_importer\ImportBatch::process',
          [$dataSourceId, $chunkOfFiles],
        ];
      }
    }
    else {
      $this->batch['operations'][] = [
        '\Drupal\remote_file_importer\ImportBatch::process',
        [$dataSourceId, $items],
      ];
    }

  }

  /**
   * Load data sources - either all or the one specified.
   *
   * @param string $dataSourceId
   *   ID of data source.
   */
  private function collectDataSources($dataSourceId) {

    if ($dataSourceId) {
      $dataSource = $this->entityTypeManager->getStorage('rfi_data_source')->load($dataSourceId);
      if ($dataSource->active) {
        $this->dataSources[$dataSourceId] = [
          'config' => $dataSource,
          'plugin' => $this->pluginManager->createInstance($dataSource->plugin_id),
        ];
        return;
      }
    }

    foreach ($this->entityTypeManager->getStorage('rfi_data_source')->loadMultiple() as $dataSourceId => $dataSource) {
      if ($dataSource->active) {
        $this->dataSources[$dataSourceId] = [
          'config' => $dataSource,
          'plugin' => $this->pluginManager->createInstance($dataSource->plugin_id),
        ];
      }
    }
  }

  /**
   * Imports from all data sources.
   *
   * @param string $dataSourceId
   *   ID of data source.
   * @param string $redirectRoute
   *   (Optional) Route to redirect after import.
   */
  public function importFiles($dataSourceId, $redirectRoute = NULL) {

    $this->collectDataSources($dataSourceId);

    foreach ($this->dataSources as $dataSourceId => $dataSource) {
      if ($dataSource['plugin']->establishConnection($dataSource['config']->settings)) {
        $filesImport = [];
        foreach ($dataSource['plugin']->collectFiles() as $file) {
          $localFileName = $this->joinPaths($dataSource['config']->destination_folder, $file['path']);
          $localFileName = $dataSource['config']->destination_base . ltrim($localFileName);
          if ($this->doUpload($localFileName, $file['metadata']['modified_date'])) {
            $dir = dirname($localFileName);
            $this->fileSystem->prepareDirectory($dir, $this->fileSystem::CREATE_DIRECTORY);
            $filesImport[] = [
              'local_file' => $localFileName,
              'remote_file' => $file['path'],
              'metadata' => $file['metadata'],
            ];
          }
        }

        $event = new FilesToImportEvent($filesImport, $dataSourceId);
        $this->eventDispatcher->dispatch($event, RemoteFileImporterEvents::FILES_TO_IMPORT);
        $filesImport = $event->getFiles();

        $this->addOperationToBatch($dataSourceId, $filesImport);
        $dataSource['plugin']->closeConnection();
      }
    }
    return $this->runBatch($redirectRoute);

  }

  /**
   * Checks whether file needs to be downloaded or not.
   *
   * @param string $localFileName
   *   Target filename.
   * @param int $remoteFileLastModified
   *   Last modified date of remote file.
   *
   * @return bool
   *   Returns whether the file needs to be downloaded or not.
   */
  private function doUpload($localFileName, $remoteFileLastModified) {
    $realPath = $this->fileSystem->realpath($localFileName);
    if (file_exists($realPath)) {
      $localFileLastModified = filemtime($realPath);
      if ($localFileLastModified > $remoteFileLastModified) {
        return FALSE;
      }
    }
    return TRUE;
  }

}
