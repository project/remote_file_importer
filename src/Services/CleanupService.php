<?php

namespace Drupal\remote_file_importer\Services;

use Drupal\Core\File\FileSystemInterface;

/**
 * CleanupService for deleting obsolete files and folder.
 */
class CleanupService {

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Constructs a new ImportService object.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system service.
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * Recursively deletes all empty subfolders within a given folder.
   *
   * @param string $directory
   *   The directory to clean.
   */
  public function deleteEmptySubfolders($directory) {
    $isEmpty = TRUE;

    // Open the directory.
    $handle = opendir($directory);

    if ($handle) {
      while (FALSE !== ($entry = readdir($handle))) {
        if ($entry == '.' || $entry == '..') {
          continue;
        }

        $fullPath = $directory . '/' . $entry;
        if (is_dir($fullPath)) {
          if ($this->deleteEmptySubfolders($fullPath)) {
            $this->fileSystem->deleteRecursive($fullPath);
          }
          else {
            $isEmpty = FALSE;
          }
        }
        else {
          $isEmpty = FALSE;
        }
      }
      closedir($handle);
    }

    return $isEmpty;
  }

}
