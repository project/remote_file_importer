<?php

namespace Drupal\remote_file_importer\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the container configuration entity.
 *
 * @ConfigEntityType(
 *   id = "rfi_data_source",
 *   label = @Translation("Data Sources for Remote File Importer"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Config\Entity\ConfigEntityStorage",
 *     "form" = {
 *       "default" = "Drupal\remote_file_importer\Form\DataSourceForm",
 *       "delete" = "Drupal\remote_file_importer\Form\DataSourceDeleteForm"
 *     },
 *     "list_builder" = "Drupal\remote_file_importer\DataSourceListBuilder"
 *   },
 *   admin_permission = "administer rfi data sources",
 *   config_prefix = "rfi_data_source",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "settings" = "settings",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "settings",
 *     "active",
 *     "sync",
 *     "keep_modify",
 *     "destination_base",
 *     "destination_folder",
 *     "plugin_id",
 *     "batch_size",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/services/remote-file-importer/add",
 *     "edit-form" = "/admin/config/services/remote-file-importer/{rfi_data_source}/edit",
 *     "delete-form" = "/admin/config/services/remote-file-importer/{rfi_data_source}/delete",
 *     "import" = "/admin/config/services/remote-file-importer/{rfi_data_source}/import",
 *   }
 * )
 */
class DataSource extends ConfigEntityBase {

  /**
   * Profile machine name.
   *
   * @var string
   */
  public $id;

  /**
   * Profile human readable name.
   *
   * @var string
   */
  public $label;

  /**
   * State, active or inactive.
   *
   * @var bool
   */
  public $active;

  /**
   * Base directory where imported files get stored.
   *
   * @var string
   */
  public $destination_base;

  /**
   * Directory where imported files get stored.
   *
   * @var string
   */
  public $destination_folder;

  /**
   * ID of the data source plugin.
   *
   * @var string
   */
  public $plugin_id;

  /**
   * Whether local folder should be synced with remote folder.
   *
   * @var bool
   */
  public $sync;

  /**
   * Whether local files should keep the last modified date of the remote files.
   *
   * @var bool
   */
  public $keep_modify;

  /**
   * Number of files to be imported per batch.
   *
   * @var int
   */
  public $batch_size;

  /**
   * The settings for this Data Source.
   *
   * @var array
   */
  public $settings;

}
