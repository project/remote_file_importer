<?php

namespace Drupal\remote_file_importer\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Defines the files to import event.
 *
 * @see \Drupal\remote_file_importer\Event\RemoteFileImporterEvents
 */
class FilesToImportEvent extends Event {

  /**
   * The files to import.
   *
   * @var array
   */
  protected $files;

  /**
   * The datasource.
   *
   * @var string
   */
  protected $datasource;

  /**
   * Constructs a new FilesToImportEvent object.
   *
   * @param array $files_to_import
   *   An array of files to import.
   * @param string $datasource
   *   The datasource.
   */
  public function __construct(array $files_to_import, $datasource) {
    $this->files = $files_to_import;
    $this->datasource = $datasource;
  }

  /**
   * Gets the files to import.
   *
   * @return array
   *   The files to import.
   */
  public function getFiles() {
    return $this->files;
  }

  /**
   * Sets the files to import.
   *
   * @param array $files
   *   The files to import.
   * 
   * @return $this
   */
  public function setFiles(array $files) {
    $this->files = $files;
    return $this;
  }

  /**
   * Gets the datasource.
   *
   * @return string
   *   The datasource.
   */
  public function getDatasource() {
    return $this->datasource;
  }

  /**
   * Sets the datasource.
   *
   * @param string $datasource
   *   The datasource.
   * 
   * @return $this
   */
  public function setDatasource($datasource) {
    $this->datasource = $datasource;
    return $this;
  }

}
