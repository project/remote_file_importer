<?php

namespace Drupal\remote_file_importer\Event;

/**
 * Defines events for the remoet file importer module.
 */
final class RemoteFileImporterEvents {

  /**
   * Name of the event fired when preparing an array of files to import.
   *
   * @Event
   *
   * @see \Drupal\remote_file_importer\Event\FilesToImportEvent
   */
  const FILES_TO_IMPORT = 'remote_file_importer.files_to_import';

}
