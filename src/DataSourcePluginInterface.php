<?php

namespace Drupal\remote_file_importer;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\remote_file_importer\Entity\DataSource;

/**
 * Defines an interface for Color plugin plugins.
 */
interface DataSourcePluginInterface extends PluginInspectionInterface {

  /**
   * Return the plugins label.
   *
   * @return string
   *   Returns the label as a string.
   */
  public function getLabel();

  /**
   * Create speficic configuration for data source.
   *
   * @return array
   *   Render array for configuration form.
   */
  public function buildConfigurationForm(array $values = []);

  /**
   * Custom validation for configuration form.
   *
   * @param array $form
   *   Render array of configuratoin form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state, containing submitted form data.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state);

  /**
   * Processes submitted form data into an array of setting.
   *
   * @param array $form
   *   Render array of configuratoin form.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state, containing submitted form data.
   * @param Drupal\remote_file_importer\Entity\DataSource $entity
   *   Data source entity.
   *
   * @return array
   *   Array containing submitted & processed form values.
   */
  public function processConfigurationForm(array &$form, FormStateInterface $form_state, DataSource $entity);

  /**
   * Login to remote service.
   *
   * @param array $settings
   *   Array of settings required to establish connection.
   *
   * @return bool
   *   Returns whether the connection has been setup successfully.
   */
  public function establishConnection(array $settings);

  /**
   * Get a list of files to be imported.
   *
   * @return array
   *   List of paths.
   */
  public function collectFiles();

  /**
   * Imports a single file.
   *
   * @param string $localFileName
   *   Target filename.
   * @param string $remoteFileName
   *   Source filename on remote service.
   *
   * @return bool
   *   Returns whether the file has been imported successfully.
   */
  public function importFile(string $localFileName, string $remoteFileName);

  /**
   * Close connection.
   */
  public function closeConnection();

}
