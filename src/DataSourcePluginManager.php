<?php

namespace Drupal\remote_file_importer;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the data source manager.
 */
class DataSourcePluginManager extends DefaultPluginManager {

  /**
   * Seetings for plugin manager.
   *
   * @var array
   */
  protected $settings;

  /**
   * Constructor for DataSourcePluginManager objects.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/RemoteFileImporter/DataSource',
      $namespaces,
      $module_handler,
      'Drupal\remote_file_importer\DataSourcePluginInterface',
      'Drupal\remote_file_importer\Annotation\RemoteFileImporterDataSourcePlugin'
    );
    $this->alterInfo('remote_file_importer_plugin_info');
    $this->setCacheBackend($cache_backend, 'remote_file_importer_plugins');
  }

}
