<?php

namespace Drupal\remote_file_importer\Commands;

use Drupal\remote_file_importer\Services\ImportService;
use Drush\Commands\DrushCommands;

/**
 * Provides ImportCommands to run imports from CLI.
 */
class ImportCommands extends DrushCommands {

  /**
   * Import Service.
   *
   * @var \Drupal\remote_file_importer\Services\ImportService
   */
  private $importService;

  /**
   * Constructs a new ImportCommands object.
   *
   * @param \Drupal\remote_file_importer\Services\ImportService $importService
   *   Import Service.
   */
  public function __construct(ImportService $importService) {
    $this->importService = $importService;
  }

  /**
   * Import files from remote data source.
   *
   * @param string $dataSourceId
   *   (optional) A data source ID, or NULL to import from all data sources.
   *
   * @command rfi:import
   *
   * @usage drush rfi:import
   *   Import files from all data sourcs.
   * @usage drush rfi:import data_source_id
   *   Import files from data source with the ID data_source_id.
   */
  public function importFiles($dataSourceId = NULL) {
    $this->importService->importFiles($dataSourceId);
  }

}
