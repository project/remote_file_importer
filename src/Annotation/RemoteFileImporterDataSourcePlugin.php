<?php

namespace Drupal\remote_file_importer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Data Source for Remote File Importer item annotation object.
 *
 * @see \Drupal\remote_file_importer\DataSourcePluginBase
 * @see plugin_api
 *
 * @Annotation
 */
class RemoteFileImporterDataSourcePlugin extends Plugin {

  /**
   * Style definition machine name.
   *
   * @var string
   */
  public $id;

  /**
   * Style definition label.
   *
   * @var string
   */
  public $label;

}
