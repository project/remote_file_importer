<?php

namespace Drupal\remote_file_importer\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route to import files from a data source.
 */
class ImportController extends ControllerBase {

  /**
   * Displays information about a search index.
   *
   * @param string $rfi_data_source
   *   ID of data source or NULL.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function import($rfi_data_source = NULL) {
    $importService = \Drupal::service('remote_file_importer.import_service');
    return $importService->importFiles($rfi_data_source, 'entity.rfi_data_source.collection');
  }

}
