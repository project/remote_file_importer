<?php

namespace Drupal\remote_file_importer;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\remote_file_importer\Entity\DataSource;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Task plugin plugins.
 *
 * @see \Drupal\remote_file_importer\Annotation\DataSourvePlugin
 * @see \Drupal\remote_file_importer\DataSourvePluginInterface
 */
abstract class DataSourcePluginBase extends PluginBase implements DataSourcePluginInterface, ContainerFactoryPluginInterface {

  use ImportHelperTrait;
  use StringTranslationTrait;

  /**
   * Connection object.
   *
   * @var Object
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TranslationInterface $string_translation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
    );
  }

  /**
   * Return the plugins label.
   *
   * @return string
   *   returns the label as a string.
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Validation is optional.
  }

  /**
   * {@inheritdoc}
   */
  public function processConfigurationForm(array &$form, FormStateInterface $form_state, DataSource $entity) {
    $settings = [];
    $settingKeys = array_keys($this->buildConfigurationForm());
    foreach ($settingKeys as $key) {
      $settings[$key] = $form_state->getValue($key);
    }
    return $settings;
  }

}
