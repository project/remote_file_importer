<?php

namespace Drupal\remote_file_importer;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Defines a listing of container configuration entities.
 *
 * @see \Drupal\redirect_after_login\Entity\Container
 */
class DataSourceListBuilder extends ConfigEntityListBuilder {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['active'] = $this->t('State');
    $header['destination_folder'] = $this->t('Destination folder');
    $header['plugin_id'] = $this->t('Data Source Type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['active'] = $entity->active ? $this->t('Active') : $this->t('Inactive');
    $row['destination_folder'] = $entity->get('destination_base') . $entity->get('destination_folder');
    $row['plugin_id'] = $entity->get('plugin_id');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    if ($entity->hasLinkTemplate('edit-form')) {
      $operations['edit'] = [
        'title' => $this->t('Edit data srouce'),
        'weight' => 20,
        'url' => $entity->toUrl('edit-form'),
      ];
    }
    if ($entity->hasLinkTemplate('import')) {
      $operations['import'] = [
        'title' => $this->t('Start import'),
        'weight' => 20,
        'url' => $entity->toUrl('import'),
      ];
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $url = Url::fromRoute('rfi_data_source.import');
    $link = Link::fromTextAndUrl($this->t('Import from all data sources'), $url)->toRenderable();
    $link['#attributes']['class'][] = 'button button--secondary';
    $build['import_all'] = $link;

    return $build;
  }

}
