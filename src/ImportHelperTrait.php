<?php

namespace Drupal\remote_file_importer;

/**
 * Manages Batch functionalities for import.
 */
trait ImportHelperTrait {

  /**
   * Joins multiple paths into a single path string.
   *
   * This function takes multiple path segments as arguments, filters out any
   * empty segments, and then concatenates them into a single path string.
   * Any duplicate slashes in the resulting path are reduced to a single slash.
   *
   * @return string
   *   The concatenated path string with normalized slashes.
   */
  public function joinPaths() {
    $paths = [];

    foreach (func_get_args() as $arg) {
      if ($arg !== '') {
        $paths[] = $arg;
      }
    }

    return preg_replace('#/+#', '/', implode('/', $paths));
  }

}
