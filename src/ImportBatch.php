<?php

namespace Drupal\remote_file_importer;

/**
 * Manages Batch functionalities for import.
 */
class ImportBatch {

  /**
   * Process the batch.
   *
   * @param string $dataSourcePluginId
   *   Id of f data source plugin.
   * @param array $items
   *   Array of items to be imported.
   *   Each item is an array with keys: local_file, remote_file.
   * @param array $context
   *   Batch context.
   */
  public static function process($dataSourcePluginId, $items, &$context) {
    $pluginManager = \Drupal::service('plugin.manager.remote_file_importer.data_source');
    $dataSource = \Drupal::entityTypeManager()->getStorage('rfi_data_source')->load($dataSourcePluginId);
    $dataSourcePlugin = $pluginManager->createInstance($dataSource->plugin_id);

    $filesLocal = [];
    if ($dataSource->sync) {
      $fileSystem = \Drupal::service('file_system');
      $filesLocal = array_keys($fileSystem->scanDirectory($dataSource->destination_base . $dataSource->destination_folder, '/.*/', ['recurse' => TRUE]));
    }

    if ($dataSourcePlugin->establishConnection($dataSource->settings)) {
      foreach ($items as $item) {
        $dataSourcePlugin->importFile($item['local_file'], $item['remote_file']);
        if ($dataSource->keep_modify) {
          $time = strtotime($item['metadata']['modified_date']);
          touch($item['local_file'], $time);
        }

        $context['results'][] = $item['local_file'];
        $filesLocal = array_diff($filesLocal, [$item['local_file']]);
      }
      $dataSourcePlugin->closeConnection();
    }

    if ($dataSource->sync) {
      // Remove files that are not in remote folder.
      foreach ($filesLocal as $file) {
        $file = $fileSystem->delete($file);
      }

      // Remove empty folders.
      $cleanUpService = \Drupal::service('remote_file_importer.cleanup_service');
      $cleanUpService->deleteEmptySubfolders($fileSystem->realpath($dataSource->destination_folder));
    }

  }

  /**
   * Called when the batch is finished.
   *
   * @param bool $success
   *   Whether the batch finished successfully.
   * @param array $results
   *   Array of results.
   * @param array $operations
   *   Array of operations.
   * @param float $elapsed
   *   Time elapsed.
   */
  public static function finished($success, $results, $operations, $elapsed) {
    if ($success) {
      $message = \Drupal::translation()
        ->formatPlural(count($results), 'One file imported.', '@count files imported.');
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()
      ->addMessage($message);
  }

}
