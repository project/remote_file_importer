# Remote File Importer

The Remote File Importer is a module designed for dynamic data synchronization from remote storage systems. This community module seamlessly integrates with Drupal, enabling automated scanning and downloading of new files from remote data sources, such as FTP storage, into Drupal’s file storage.

For a full description of the module, visit the [project page](https://www.drupal.org/project/remote_file_importer).

## Installation
Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-modules). The core module of the Remote File Importer itself does not provide any interfaces to data sources; they are all available via submodules::
- `remote_file_importer_ftp`: Data source to securely connect to remote storage using FTP or FTPS, configuring hostname, port, user credentials, and source folder.


## Configuration

This module provides a GUI to manage datasources and start imports. The GUI is available at "Configuration » Web Services » Remote File Importer".

### Drush commands
This module also provides a set of drush commands to interact with configured data sources:

`drush rfi:import` \
Import files from all data sourcs.

`drush rfi:import data_source_id`\
  Import files from data source with the ID data_source_id.
