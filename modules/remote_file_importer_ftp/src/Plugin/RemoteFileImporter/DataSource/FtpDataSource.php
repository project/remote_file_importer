<?php

namespace Drupal\remote_file_importer_ftp\Plugin\RemoteFileImporter\Datasource;

use Drupal\Core\Form\FormStateInterface;
use Drupal\remote_file_importer\DataSourcePluginBase;
use Drupal\remote_file_importer\Entity\DataSource;

/**
 * Provids FTP import functionality.
 *
 * @RemoteFileImporterDataSourcePlugin(
 *   id = "rif_ftp_datasource",
 *   label = @Translation("FTP"),
 * )
 */
class FtpDataSource extends DataSourcePluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm($values = []) {

    $form = [];
    $form['host'] = [
      '#type' => 'textfield',
      '#default_value' => $values['host'] ?? '',
      '#title' => $this->t('Host'),
      '#description' => $this->t('Exclude protocol.'),
      '#required' => TRUE,
    ];

    $form['port'] = [
      '#type' => 'textfield',
      '#default_value' => $values['port'] ?? '',
      '#title' => $this->t('Port'),
      '#placeholder' => '21',
    ];

    $form['ssl'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('SSL / TLS'),
      '#default_value' => $values['ssl'] ?? TRUE,
    ];

    $form['ftp_mode'] = [
      '#type' => 'select',
      '#default_value' => $values['select'] ?? '',
      '#title' => $this->t('Mode'),
      '#options' => [
        'passive' => $this->t('Passive'),
        'active' => $this->t('Active'),
      ],
    ];

    $form['user'] = [
      '#type' => 'textfield',
      '#default_value' => $values['user'] ?? '',
      '#title' => $this->t('User'),
      '#required' => TRUE,
    ];

    $new = empty($values['host']);
    $form['change_pass'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Change password'),
      '#default_value' => $new,
      '#access' => !$new,
      '#description' => $this->t('Your password is stored; select to change it.'),
    ];

    $form['password'] = [
      '#type' => 'password',
      '#default_value' => $values['password'] ?? '',
      '#title' => $this->t('Password'),
      '#required' => $new,
      '#states' => [
        'visible' => [
          ':input[name="change_pass"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['remote_dir'] = [
      '#type' => 'textfield',
      '#default_value' => $values['remote_dir'] ?? '',
      '#title' => $this->t('Remote Dir'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getValue('host');
    if ($input) {
      if (strpos($input, ':') !== FALSE || strpos($input, '//') !== FALSE) {
        $form_state->setErrorByName('host', $this->t('The host must not contain the protocol (e.g. ftp://).'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function establishConnection($settings) {

    $port = !empty($settings['port']) ? $settings['port'] : 21;

    if ($settings['ssl']) {
      $this->connection = ftp_ssl_connect($settings['host'], $port);
    }
    else {
      $this->connection = ftp_connect($settings['host'], $port);
    }
    if (ftp_login($this->connection, $settings['user'], $settings['password'])) {
      if ($settings['ftp_mode'] == 'passive') {
        ftp_pasv($this->connection, TRUE);
      }
      if ($settings['remote_dir']) {
        ftp_chdir($this->connection, $settings['remote_dir']);
      }
      
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function collectFiles() {
    $files = $this->ftpGetAllFiles(".");
    return $files;
  }

  /**
   * Gets all remote files recursively.
   *
   * @param string $directory
   *   Remote directory to be scanned.
   *
   * @return array
   *   List of paths.
   */
  private function ftpGetAllFiles(string $directory): array {
    $files = [];
    $items = ftp_mlsd($this->connection, $directory);
    foreach ($items as $item) {
      $itemName = $item['name'];
      if ($itemName == '.' || $itemName == '..') {
        continue;
      }

      $fullPath = $this->joinPaths($directory, $itemName);
      if ($item['type'] == 'dir') {
        $files = array_merge($files, $this->ftpGetAllFiles($fullPath));
      }
      else {
        $files[] = [
          'path' => ltrim($fullPath, "./"),
          'metadata' => [
            'modified_date' => $item['modify'],
          ],
        ];
      }
    }

    return $files;
  }

  /**
   * {@inheritdoc}
   */
  public function importFile($localFileName, $remoteFileName) {
    ftp_get($this->connection, $localFileName, $remoteFileName);
  }

  /**
   * {@inheritdoc}
   */
  public function closeConnection() {
    $result = ftp_close($this->connection);
  }

  /**
   * {@inheritdoc}
   */
  public function processConfigurationForm(array &$form, FormStateInterface $form_state, DataSource $entity) {
    $settings = [];
    $settingKeys = array_keys($this->buildConfigurationForm());
    foreach ($settingKeys as $key) {
      $settings[$key] = $form_state->getValue($key);
    }

    if (!$form_state->getValue('change_pass') && $entity->get('settings')) {
      $settings['password'] = $entity->get('settings')['password'];
    }

    if (array_key_exists('change_pass', $settings)) {
      unset($settings['change_pass']);
    }

    return $settings;
  }

}
